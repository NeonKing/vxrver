/*
    Minimal FreeFrame Linux Host
    Copyright (c) 2004, Gabor Papp <it3paga at ituniv se>
    All rights reserved.
    
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston,
    MA  02111-1307, USA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <SDL/SDL.h>

#include "FreeFrame.h"
#include "ffhost.h"
#include "ffload.h"

static SDL_Surface *screen;
static SDL_Surface *screen_surface;

static void *pixels;

unsigned char keytable[512] = {0,};

void panic(const char *panicstr, ...)
{
    char msgbuf[256];

    va_list args;

    va_start(args, panicstr);
    vsnprintf(msgbuf, 256, panicstr, args);
    va_end(args);

    fprintf(stderr, "%s\n", msgbuf);
    
    exit(1);
}

int init(void)
{
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0)
	return 1;
    
    atexit(SDL_Quit);
    
    pixels = malloc(XSIZE*YSIZE*(V_BITS >> 3));
    
    screen = SDL_SetVideoMode(XSIZE, YSIZE, V_BITS, SDL_DOUBLEBUF | SDL_HWSURFACE);    
    
    screen_surface = SDL_CreateRGBSurfaceFrom(pixels, XSIZE, YSIZE, V_BITS, XSIZE*(V_BITS >> 3), 
	#if (V_BITS == 16)
	    0x0000f800, 0x000007e0, 0x0000001f, 0x00000000
	#else // V_BITS = 32 or 24
	    0x00ff0000, 0x0000ff00, 0x000000ff, 0x00000000
	#endif
	);

    if ((pixels == NULL) || (screen == NULL) || (screen_surface == NULL))
	return 1;

    return 0;
}

void testimage(void)
{
    int time = (int)(SDL_GetTicks()*.023);

    unsigned char *p = (unsigned char *)pixels;

    for (int y=0; y<YSIZE; y++)
    {
	for (int x=0; x<XSIZE; x++)
	{
	    int r = ((x^(y+time)) & 0xff);
	    int g = (((x+time)^y) & 0xff);
	    int b = ((x^y) & 0xff);

	#if (V_BITS == 16)
	    *((unsigned short *)p) = (unsigned short)(((r & 0xf8) << 8) |
			    ((g & 0xfc) << 3) |
			    ((b & 0xf8) >> 3));
	#else // V_BITS = 32 or 24
	    *p = b;
	    *(p+1) = g;
	    *(p+2) = r;
	#endif
	    p += (V_BITS >> 3);
	}
    }
}

void mainloop(void)
{
    int current_effect = 0;
    
    do
    {
	testimage();
	
	if (current_effect > 0)
	{
	    ff_processframe(current_effect-1, pixels);
	    SDL_WM_SetCaption(plugins[current_effect-1].name, NULL);
	    ff_control(current_effect-1);
	}
	else
	{
	    SDL_WM_SetCaption("source", NULL);
	}
	
	SDL_Surface *outputsurface = SDL_DisplayFormat(screen_surface);
	SDL_BlitSurface(outputsurface, NULL, screen, NULL);
	
	SDL_Flip(screen);
	SDL_FreeSurface(outputsurface);

	if (keytable[SDLK_LEFT])
	{
	    current_effect = (current_effect - 1 + (plugin_count + 1)) % (plugin_count + 1);
	    keytable[SDLK_LEFT] = 0;
	}
	else
	if (keytable[SDLK_RIGHT])
	{
	    current_effect = (current_effect + 1) % (plugin_count + 1);
	    keytable[SDLK_RIGHT] = 0;
	}
	
	SDL_Event event;
	while(SDL_PollEvent(&event))
	{
	    SDLKey sym = event.key.keysym.sym;

	    switch(event.type)
	    {
		case SDL_KEYDOWN:
		    keytable[sym] = 1;
		
		    if (sym == SDLK_RETURN &&
			(event.key.keysym.mod & KMOD_ALT))
		    {
			SDL_WM_ToggleFullScreen(screen);
			if (SDL_GetVideoSurface()->flags & SDL_FULLSCREEN)
			    SDL_ShowCursor(SDL_DISABLE);
			else
			    SDL_ShowCursor(SDL_ENABLE);
		    }
		    break;
		case SDL_KEYUP:
		    keytable[sym] = 0;
		    break;
		case SDL_QUIT:
		    keytable[SDLK_ESCAPE] = 1;
		    break;
	    }
	}
    }while(!keytable[SDLK_ESCAPE]);
}

int main(void)
{
    if (init())
	panic("unable to initialize SDL: %s", SDL_GetError());
    
    ff_loadplugins(PLUGINDIR);
    
    mainloop();
    
    ff_freeplugins();
    
    return 0;
}
