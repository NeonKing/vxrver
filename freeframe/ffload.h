/*
    Minimal FreeFrame Linux Host
    Copyright (c) 2004, Gabor Papp
    All rights reserved.
    
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston,
    MA  02111-1307, USA.
*/

#ifndef __FFLOAD_H__
#define __FFLOAD_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
    char	name[17];
    char	id[5];
    plugMainType *plugmain;
    unsigned	instance;
    unsigned	numparameters;
} PLUGIN;

extern void ff_loadplugins(char *plugindir);
extern void ff_processframe(int plugin, void *buffer);
extern void ff_control(int plugin);
extern void ff_freeplugins(void);

extern PLUGIN *plugins;
extern int plugin_count;

#ifdef __cplusplus
}
#endif

#endif
