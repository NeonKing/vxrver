/*
    Minimal FreeFrame Linux Host
    Copyright (c) 2004, Gabor Papp <it3paga at ituniv se>
    All rights reserved.
    
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston,
    MA  02111-1307, USA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <limits.h>
#include <dlfcn.h>

#include "FreeFrame.h"
#include "ffhost.h"
#include "ffload.h"

#include <SDL/SDL.h>

static struct dirent **filename_list;
static int filename_count;

PLUGIN *plugins;
int plugin_count;

static int selector(const struct dirent *dp)
{    
    return (strstr(dp->d_name, ".so") != NULL);
}

static void scan_plugins(char *plugindir)
{
    filename_count = scandir(plugindir, &filename_list, selector, alphasort);
    if (filename_count < 0)
       filename_count = 0;
}

void ff_loadplugins(char *plugindir)
{
    char libname[PATH_MAX];
    plugMainType *plugmain;
    unsigned instance, numparameters;

    scan_plugins(plugindir);

    plugins = (PLUGIN *)malloc(filename_count*sizeof(PLUGIN));
    if (plugins == NULL)
	panic("no memory for loading plugins\n");
    plugin_count = 0;
    
    for (int i=0; i<filename_count; i++)
    {
	char *pluginname = filename_list[i]->d_name;
	
	snprintf(libname, PATH_MAX, "%s/%s", plugindir, pluginname);
	
	void *plugin_handle = dlopen(libname, RTLD_NOW);
	dlerror();
	plugmain = (plugMainType *)(unsigned)dlsym(plugin_handle, "plugMain");
	if (plugmain == NULL)
	    panic("plugin %s: %s", filename_list[i]->d_name, dlerror());
	
	PlugInfoStruct *pis = (plugmain(FF_GETINFO, NULL, 0)).PISvalue;

	if ((plugmain(FF_GETPLUGINCAPS, (LPVOID)FF_CAP_V_BITS_VIDEO, 0)).ivalue != FF_TRUE)
	    panic("plugin %s: no %dbit support", V_BITS, pluginname);

	if (pis->APIMajorVersion < 1)
	    panic("plugin %s: old api version", pluginname);
	
	if ((plugmain(FF_INITIALISE, NULL, 0)).ivalue == FF_FAIL)
	    panic("plugin %s: init failed", pluginname);
	
	VideoInfoStruct vidinfo;
	vidinfo.frameWidth = XSIZE;
	vidinfo.frameHeight = YSIZE;
	vidinfo.orientation = 1;
	vidinfo.bitDepth = FF_CAP_V_BITS_VIDEO;

	instance = plugmain(FF_INSTANTIATE, &vidinfo, 0).ivalue;
	if (instance == FF_FAIL)
	    panic("plugin %s: init failed",  pluginname);
	
	numparameters = plugmain(FF_GETNUMPARAMETERS, NULL, 0).ivalue;
	if (numparameters == FF_FAIL)
	    panic("plugin %s: numparameters failed",  pluginname);
	
	plugins[plugin_count].plugmain = plugmain;
	
	strncpy(plugins[plugin_count].name, (char *)(pis->pluginName), 16);
	strncpy(plugins[plugin_count].id, (char *)(pis->uniqueID), 4);
	plugins[plugin_count].name[16] = 0;
	plugins[plugin_count].id[4] = 0;
	
	plugins[plugin_count].instance = instance;
	plugins[plugin_count].numparameters = numparameters;
	
	printf("%s [%s] is loaded\n", plugins[plugin_count].name, pluginname);
	plugin_count++;
    }
}


void ff_processframe(int plugin, void *buffer)
{
    plugins[plugin].plugmain(FF_PROCESSFRAME, buffer,  plugins[plugin].instance);
}

void ff_control(int plugin)
{
    plugMainType *plugmain = plugins[plugin].plugmain;
    unsigned instance = plugins[plugin].instance;
    unsigned numparameters = plugins[plugin].numparameters;
    
    int keys[] = {SDLK_1, SDLK_q, SDLK_2, SDLK_w, SDLK_3, SDLK_e,
		SDLK_4, SDLK_r, SDLK_5, SDLK_t, SDLK_6, SDLK_y,
		SDLK_7, SDLK_u, SDLK_8, SDLK_i, SDLK_9, SDLK_o,
		SDLK_0, SDLK_p};
    
    if (numparameters > sizeof(keys)/sizeof(keys[0])/2)
	numparameters = sizeof(keys)/sizeof(keys[0])/2;

    SetParameterStruct sps;
    
    for (int i=0; i<numparameters; i++)
    {
	if (!keytable[keys[i*2]] && !keytable[keys[i*2+1]])
	    continue;

	if (plugmain(FF_GETPARAMETERTYPE, (LPVOID)i, 0).ivalue == FF_TYPE_TEXT)
	    continue;

	sps.value = plugmain(FF_GETPARAMETER, (LPVOID)i, instance).fvalue;
	
	if (keytable[keys[i*2]])
	    sps.value += .05;
	if (keytable[keys[i*2+1]])
	    sps.value -= .05;
	
	if (sps.value > 1.0) sps.value = 1.0;
	else
	if (sps.value < 0.0) sps.value = 0.0;
	    
	sps.index = i;
	plugmain(FF_SETPARAMETER, &sps, instance);
    }
}

void ff_freeplugins(void)
{
    for (int i=0; i<plugin_count; i++)
    {
	plugMainType *plugmain = plugins[i].plugmain;
    
	plugmain(FF_DEINSTANTIATE, NULL, plugins[i].instance);
	plugmain(FF_DEINITIALISE, NULL, 0);
    }
    free(plugins);
}
