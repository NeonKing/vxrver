/*
    Minimal FreeFrame Linux Host
    Copyright (c) 2004, Gabor Papp
    All rights reserved.
    
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston,
    MA  02111-1307, USA.
*/

#ifndef __FFHOST_H__
#define __FFHOST_H__

#ifndef XSIZE
#define XSIZE 640
#endif

#ifndef YSIZE
#define YSIZE 480
#endif

#ifndef V_BITS
#define V_BITS 32 // 32, 24 or 16
#endif

#define PLUGINDIR "plugins"

#if (V_BITS == 32)
#define FF_CAP_V_BITS_VIDEO	FF_CAP_32BITVIDEO
#elif (V_BITS == 24)
#define FF_CAP_V_BITS_VIDEO	FF_CAP_24BITVIDEO
#else // V_BITS = 16
#define FF_CAP_V_BITS_VIDEO	FF_CAP_16BITVIDEO
#endif

#ifdef __cplusplus
extern "C" {
#endif

extern void panic(const char *panicstr, ...);
extern unsigned char keytable[512];

#ifdef __cplusplus
}
#endif

#endif
